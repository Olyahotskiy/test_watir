module JacketSelectors
  class JacketPage
    include PageObject

    link :brand_element, href: 'https://shift.ua/wear/jacket/element'
    link :spring_bismark, href: 'https://shift.ua/wear/jacket/kurtka-element-spring-14-bismark-total-eclipse'
    span :special_price, id: 'product-price-418'
    button :card_button, class: 'button btn-cart'
    link :preview_button, class: 'qs-button cdz-tooltip'
    button :add_to_cart, id: 'product-addtocart-button'
    link :l_size, id: 'swatch423'
    span :selected_size, id: 'select_label_size'
    button :order_button, css: 'div[class="check-out"] button'
    element :image_added_jacket, xpath: "//span[@class='product-image']/img"
    span :total_price, class: 'price'
    h2 :product_name, class: 'product-name'
  end
end
