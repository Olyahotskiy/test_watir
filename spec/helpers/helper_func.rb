module HelperFunctions
  def wait_for_visible(element)
    sleep 1 unless element.visible?
  end
end
