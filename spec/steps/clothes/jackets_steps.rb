describe 'test clothes tab' do
  let(:browser) { JacketSelectors::JacketPage.new @browser }

  before(:all) do
    @jacket_name = 'Куртка Element Spring 14 BISMARK TOTAL ECLIPSE'
    @jacket_price = '869 грн.'
  end

  it 'open page' do
    browser.goto ENV.fetch('PROJECT_URL')
    expect(browser.brand_element_element.visible?).to be_truthy
  end

  it 'open brand Element' do
    browser.brand_element
    expect(browser.spring_bismark_element.title).to eql @jacket_name
  end

  it 'Hover over the item and open previw page' do
    browser.spring_bismark_element.hover
    browser.preview_button_element.wait_until(10, &:visible?)
    expect(browser.preview_button_element.visible?).to be_truthy
    browser.preview_button_element.click
  end

 it 'Add item to the cart on the preview page' do
   wait_for_visible(browser.add_to_cart_element)
   expect(browser.add_to_cart_element.visible?).to be_truthy
   browser.l_size
   expect(browser.selected_size_element.text).to eql 'L'
   browser.add_to_cart
 end

  it 'Go to the cart checkout page verify' do
    browser.image_added_jacket_element.wait_until(50, &:present?)
    expect(browser.order_button_element.present?).to be_truthy
    browser.order_button
  end

  it 'Check item name and price' do
    browser.total_price_element.wait_until(50, &:present?)
    expect(browser.total_price_element.text).to eql @jacket_price.upcase
    expect(browser.product_name_element.text).to eql @jacket_name
  end
end
