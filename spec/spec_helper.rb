# frozen_string_literal: true

require 'selenium-webdriver'
require 'page-object'
require 'dotenv'
require 'pry'
require 'watir'

Dotenv.load '.env'

browser = Watir::Browser.new :chrome, headless: false
browser.driver.manage.window.maximize

require './spec/selectors/clothes/jacket_page'
require 'helpers/helper_func'
include HelperFunctions

Watir.default_timeout = 50

RSpec.configure do |config|
  config.before(:all) { @browser ||= browser }
end
